# LiveDraw
一个可以让你在屏幕上实时绘制的工具。

## 前言
When you need to draw or mark something with presentation, you may use some tools like
[Windows Ink Workspace](https://blogs.windows.com/windowsexperience/2016/10/10/windows-10-tip-getting-started-with-the-windows-ink-workspace/),
but all of them are actually **taking a screenshot** and allowing you to draw on it.
That's actually annoying when you want to presentation something dynamic.

However, **LiveDraw is here and built for it!**

当你在演示介绍的时候，想要绘制或标记某些东西，你也许需要用到类似 [Windows Ink工作区](https://blogs.windows.com/windowsexperience/2016/10/10/windows-10-tip-getting-started-with-the-windows-ink-workspace/) 这样的工具，但是它们实际都是**截取屏幕**，并允许您在上面绘制。当您想要展示一些动态的东西的时候，这真的很烦人。

一切都好了，**LiveDraw来了，为此而生！**

## 界面
![](screenshots/00.png)

![](screenshots/01.png)

![](screenshots/02.png)

## 用法
The shortcuts that can be used（可用的快捷键）:
- [ Z ]  Undo撤销, [ Y ]  Redo重做,
- [ E ]  Eraser By Stroke按笔画擦除, [ D ]  Eraser By Point按点擦除,
- [ R ]  Release or Recover interface,释放或恢复界面
- [ + ]  Increase size brush笔刷变大, [ - ]  Decrease size brush笔刷变小
- [ B ]  Brush mode涂鸦模式, [ L ]  Line Mode画线模式

### 下载
[Release](https://github.com/antfu/live-draw/releases)

### 系统需求
- Windows OS
- .Net Core 5 x86/x64 

### 功能特色
- True transparent background (you can draw freely even when you are watching videos).透明背景
- Select colors by simply clicks.单击选色
- Adjust the size of brush.笔刷大小可调
- Draw ink with pressure (when using a pen with pen pressure).支持压力笔
- Auto smooth ink.自动平滑笔画
- Mini mode.迷你模式
- Unlimited undo/redo.无限撤销重做
- Freely drag the palette.自由拖动调色板
- Save and load ink to file (binary file) with color.墨迹可存取（附带颜色）
- Temporarily disable draw then you can be able to operate other windows.暂时禁用绘制，可操作其它窗口
- Fully animated.

## 待办事项
- Edge docking
- Export as image files 导出为图像文件
- Mouse penetration 鼠标穿透

## 发布
- dotnet publish -c Release -r win-x86 -p:PublishSingleFile=true
- dotnet publish -c Release -r win-x64 -p:PublishSingleFile=true

## 许可
MIT
